# Blog Application
## The Blog Developed Using Django.


This web application is created using Django (Python web framework). It is a Dynamic Blog. Bootstrap, HTML, CSS, Javascript is used in the frontend of the project. Backend is developed on Django(Python). It is beautifully designed and have many features like authentication system, comments and reply system, custom rich text-editor for writing blog post, search functionality, contact form, etc.


## Features of Project

* Beautiful Design of Webpages
* Dynamic Content
* Search Functionality
* Authentication System(Sign In and Sign Up)
* Comments and Reply System on Blog Posts
* Facebook like, share plugin
* Contact Form
* Custom Rich Text-editor Integrated for Content Writing
